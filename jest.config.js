module.exports = {
  testEnvironment: 'node',
  reporters: ['default', 'jest-html-reporters'],
  collectCoverage: true,
  };
  